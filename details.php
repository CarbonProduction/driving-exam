<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
			<!-- content -->
			<a href="result.php" class="btn-secondary">Back to Result</a>
			<div class="details">
				<div class="tab-wrap">
					<a href="javascript:void(0);" class="passed-tab active">
						Correct
					</a href="javascript:void(0);">
					<a href="javascript:void(0);" class="failed-tab">
						Wrong
					</a href="javascript:void(0);">
				</div>

				<div class="details-wrap">
					<div class="passed-holder active-holder">
						<ul class="question-list">
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder passed ">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>


						</ul>
						<div class="column-noborder">
							<div class="col-half">
								<!-- kapag less than 10 yung question nalalagyan siya automatically ng class na btn-stop -->
								<a href="javascript:void(0);" class="btn-secondary prev-list">Back</a>
							</div>
							<div class="col-half">
								<!-- kapag less than 10 yung question nalalagyan siya automatically ng class na btn-stop -->
								<a href="javascript:void(0);" class="btn-main next-list">Next</a>

							</div>
						</div>	
					</div>

					<div class="failed-holder">
						<ul class="question-list">
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>
							<li class="question-holder failed">
								<p>
									What will happen when your front tire blows out?
								</p>

								<div class="status correct-ans">
									<span>Correct Answer: </span>
									<strong>A</strong>
									<p>The back end will sway towards the side of the blowout</p>
								</div>
							</li>

						</ul>
						<div class="column-noborder">
							<div class="col-half">
								<!-- kapag less than 10 yung question nalalagyan siya automatically ng class na btn-stop -->
								<a href="javascript:void(0);" class="btn-secondary prev-list">Back</a>
							</div>
							<div class="col-half">
								<!-- kapag less than 10 yung question nalalagyan siya automatically ng class na btn-stop -->
								<a href="javascript:void(0);" class="btn-main next-list">Next</a>

							</div>
						</div>
					</div>
						
				</div>
				
			</div>
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>
