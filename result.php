<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		<div class="date-validate" id="dtBox"></div>
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div id="page-result" class="mainwrap">

			<!-- content -->
			<div class="result-wrap">

				<div class="result-cont">
					<h1 class="exam-alert passed">You Passed!</h1>
					<h1 class="points passed">80 <strong class="passed-small">/100</strong></h1>
					<div class="button-wrap">
						<a href="details.php" class="btn-main">View Details</a>
					</div>
				</div>
					<div class="column">
						<h4 class="main-color">Schedule:</h4>
						<form action="">
							<div class="column-noborder">		
								<div class="col-half">

									<label for="">
										Date:
									</label>
									<input class="dateresult" id="date-selected" type="text" placeholder="mm/dd/yy" data-field="date" readonly>
									<span id="date-invalid"></span>	
								</div>
								<div class="col-half">
									<label for="">
										Branch:
									</label>
									<select name="" id="">
										<option value="">-Select Branch-</option>
										<option value="">Manila</option>
										<option value="">Quezon City</option>
									</select>			
								</div>
							</div>
						</div>
						<div class="app-number">
							<label for="">
								<strong>
									Application Number:
								</strong> 
								<span>12345678</span>
							</label>
						</div>
						<div class="button-wrap">
							<input type="submit" class="btn-secondary">	
						</div>
					</form>	
				</div>
			</div>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>
