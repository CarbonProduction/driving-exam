<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		<div id="dtBox"></div>
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap page-failed">

			<!-- content -->
			<div class="result-wrap">

				<div class="result-cont">
					<h1 class="exam-alert failed">You Failed!</h1>
					
					<h1 class="points failed">30 <strong class="failed-small">/100</strong></h1>

					<div class="button-wrap">
						<a href="details.php" class="btn-main">View Details</a>
					</div>

					<p class="warning">You may retake your exam after 5 days</p>
					<a href="http://www.lto.gov.ph/" class="default-link">Proceed to LTO Official Website</a>
				</div>
			</div>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>
