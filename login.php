<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap page-index">

			<!-- content -->
			<div class="container">
				<div class="login">
				<h1>Driver's License Written Examination</h1>
					<form action="">
						<div class="input">
							<div class="input-label">Username</div>
							<input type="text">
						</div>
						<div class="input">
							<div class="input-label">Password</div>
							<input type="text">
						</div>
						<a class="register-link" href="register.php">Register Now</a>
						<button type="submit" class="btn-main">Login</button>
					</form>
				</div>
			</div>
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

