$(window).resize(function(){
  check_pop_ht();
});

// hide popup "popClose()"
function popClose() {

  $('#'+pop_default_id).empty();
  $('.popup-mask, .popup-container, .popup').fadeOut();
}

$(document).on('click', '.js-close', function(){
  popClose();
});

//DATEPICKER
$("#dtBox").DateTimePicker({
  dateSeparator: "/",
  dateFormat: "MM/dd/yyyy",
  timeFormat: "hh:mm AA"
});

// DETAILS TAB PAGE
$('.passed-tab').on('click', function() {
  $(this).addClass('active');
  $(this).siblings().removeClass('active');
  $('.passed-holder').addClass('active-holder').css({'display':'block'});
  $('.passed-holder').siblings().removeClass('active-holder');
});

$('.failed-tab').on('click', function() {
  $(this).addClass('active');
  $(this).siblings().removeClass('active');
  $('.failed-holder').addClass('active-holder');
  $('.failed-holder').siblings().removeClass('active-holder').css({'display':'none'});
});

// EXAM BUTTON
function forExamPage(parentType) {

  $(document).on('ready',function(){
    
    $('.'+parentType+" .question-list .question-holder:gt(9)").hide();

    var quest = $('.'+parentType+" .question-list .question-holder").length;
    
    for(var i = 0; i < quest; i++) {
      $('.'+parentType+" .question-list .question-holder").eq(i).find('p').prepend(i + 1 + '.');
      
      if(i < 10) {
        $('.'+parentType+" .next-list").addClass('btn-stop');
        $('.'+parentType+" .prev-list").addClass('btn-stop');
        
      } else {
        $('.'+parentType+" .next-list").removeClass('btn-stop');
        $('.'+parentType+" .prev-list").removeClass('btn-stop');
      }
    };

    if(quest <= 10) {
      $('<button type="submit" name="submit" id="exam-submit" class="btn-main">Submit</button>').insertAfter($('.'+parentType+" .next-list"));
      $('.'+parentType+" .next-list").remove();
    }

    $('.'+parentType+" .prev-list").addClass('btn-stop');

  });

  $('.'+parentType+" .prev-list").on('click',function() {
    var firstList = $('.' +parentType+ " .question-list").children('.question-holder:visible:first');
    firstList.prevAll(':lt(10)').show();
    firstList.prev().nextAll().hide();

    if($('.'+parentType+" .question-list").children('.question-holder:visible:first')) {
      $('.' +parentType+ " .next-list").siblings('.btn-main').remove();
      $('.' +parentType+ " .next-list").show();
    }

    if($('.'+parentType+" .question-list").children('.question-holder').first().css('display') == 'list-item') {
      $('.' +parentType+ " .prev-list").addClass('btn-stop');
    }

  });

  $('.'+parentType+ " .next-list").on('click',function() {
    var lastList = $('.'+parentType+ " .question-list").children('.question-holder:visible:last');
    lastList.nextAll(':lt(10)').show();
    lastList.next().prevAll().hide();
    $('.'+parentType+" .prev-list").removeClass('btn-stop');

    if($('.'+parentType+" .question-list").children('.question-holder').last().css('display') == 'list-item') {
      
      $('<button type="submit" name="submit" id="exam-submit" class="btn-main">Submit</button>').insertAfter($('.'+parentType+" .next-list"));
      $('.'+parentType+" .next-list").hide();

    }

  });
}

function forDetailsPage(parents) {

  $(document).on('ready',function(){

    
    $('.'+parents+" .question-list .question-holder:gt(9)").hide();

    var quest = $('.'+parents+" .question-list .question-holder").length;
    
    for(var i = 0; i < quest; i++) {
      $('.'+parents+" .question-list .question-holder").eq(i).find('p:first').prepend(i + 1 + '.');
      
      if(i < 10) {
        $('.'+parents+ ".next-list").addClass('btn-stop');
        $('.'+parents+ " .prev-list").addClass('btn-stop');
        
      } else {
        $('.'+parents+ ".next-list").removeClass('btn-stop');
        $('.'+parents+ " .prev-list").removeClass('btn-stop');
      }
    };

    if(quest <= 10) {
      $('.'+parents+ " .next-list").addClass('btn-stop');
    }

    $('.'+parents+ " .prev-list").addClass('btn-stop');

  });

  $('.'+parents+" .prev-list").on('click',function() {
    var firstList = $('.' +parents+ " .question-list").children('.question-holder:visible:first');
    firstList.prevAll(':lt(10)').show();
    firstList.prev().nextAll().hide();

    if($('.'+parents+" .question-list").children('.question-holder:visible:first')) {
      $('.' +parents+ " .next-list").siblings('.btn-main').remove();
      $('.' +parents+ " .next-list").removeClass('btn-stop');
    }

    if($('.'+parents+" .question-list").children('.question-holder').first().css('display') == 'list-item') {
      $('.' +parents+ " .prev-list").addClass('btn-stop');
    }

  });

  $('.'+parents+ " .next-list").on('click',function() {
    var lastList = $('.'+parents+ " .question-list").children('.question-holder:visible:last');
    lastList.nextAll(':lt(10)').show();
    lastList.next().prevAll().hide();
    $('.'+parents+" .prev-list").removeClass('btn-stop');

    if($('.'+parents+" .question-list").children('.question-holder').last().css('display') == 'list-item') {
      $('.'+parents+ " .next-list").addClass('btn-stop');
    }

  });
}

forExamPage('question-wrap');

forDetailsPage('passed-holder');

forDetailsPage('failed-holder');