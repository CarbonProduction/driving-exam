<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		<div id="dtBox"></div>
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap page-register">

			<!-- content -->
			<div class="container">
				<div class="register">
					<h1>Registration</h1>
					<form action="">
						<div class="input">
							<div class="input-label">First Name</div>
							<input type="text">
						</div>
						<div class="input">
							<div class="input-label">Last Name</div>
							<input type="text">
						</div>
						<div class="input">
							<div class="input-label">Email</div>
							<input type="email">
						</div>
						<div class="input">
							<div class="input-label">Mobile Number</div>
							<input type="number" placeholder="ex. 0999*******">
						</div>
						<div class="input">
							<div class="input-label">Birthday</div>
							<input type="text" placeholder="mm/dd/yy" data-field="date" readonly>
						</div>
						<div class="input">
							<div class="input-label">Restriction</div>
							<div class="checkbox">
								<div class="checkbox-label">Type 1 (2 wheels vehicle)</div>
								<input type="checkbox" name="restriction">
							</div>
							<div class="checkbox">
								<div class="checkbox-label">Type 2 (4 wheels vehicle)</div>
								<input type="checkbox" name="restriction">
							</div>
						</div>
						<div class="input">
							<div class="input-label">License Type</div>
							<div class="checkbox">
								<div class="checkbox-label">Non-professional</div>
								<input type="radio" name="type">
							</div>
							<div class="checkbox">
								<div class="checkbox-label">Professional</div>
								<input type="radio" name="type">
							</div>
						</div>
						<button type="submit" class="btn-main">Submit</button>
						
					</form>
				</div>
			</div>
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

