<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap">
			<!-- content -->
			<div class="question-wrap">
				<!-- <div class="breadcrumbs">
					5 out of 10 questions
				</div> -->
				<form action="">
					<ul class="question-list">
						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
>>>>>>> chris
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
<<<<<<< HEAD
<<<<<<< HEAD
=======
=======
>>>>>>> chris
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
<<<<<<< HEAD
>>>>>>> chris
=======
>>>>>>> chris
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
<<<<<<< HEAD
<<<<<<< HEAD
							 What will happen when your front tire blows out?
=======
=======
>>>>>>> chris
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
>>>>>>> chris
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
<<<<<<< HEAD
<<<<<<< HEAD
						

=======
=======
>>>>>>> chris

						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
							 What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>

						<!--  -->
						<li class="question-holder">
							<p>
								<span>
									What will happen when your front tire blows out?
								</span>
								<!-- <div class="img-wrap"> -->
									<img src="dist/images/sign.png" alt="">
								<!-- </div> -->
							</p>
							

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
						
						<!--  -->
						<li class="question-holder">
							<p>
								What will happen when your front tire blows out?
							</p>

							<ul class="choices">
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway towards the side of the blowout</span>
										</label>
									</div>
								</li>
								
								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The back end will sway away from the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull towards the side of the blowout</span>
										</label>
									</div>
								</li>

								<li>
									<div class="type">
										<label>
											<input type="radio" name="radio" value=""/>
											<span>The front end will pull to the opposite side of the blowout</span>
										</label>
									</div>
								</li>
							</ul>
						</li>
					</ul>

					<div class="column-noborder">
						<div class="col-half">
							<!-- kapag less than 10 yung question nalalagyan siya automatically ng class na btn-stop -->
							<a href="javascript:void(0);" class="btn-secondary prev-list">Back</a>
						</div>
						<div class="col-half">
							<!-- kapag less than 10 yung question nalalagyan siya automatically ng class na btn-stop -->
							<a href="javascript:void(0);" class="btn-main next-list">Next</a>
						</div>
					</div>

					
				</form>
			</div>
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>

