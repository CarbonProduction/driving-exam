<!doctype html>
<html lang="en">
	<head>
		<?php include('includes/sources.php'); ?>
	</head>
	<body>
		<!-- PRELOADER -->
		<?php include('includes/progress.php'); ?>
		
		<!-- HEADER -->
		<header>
			<?php include('includes/header.php'); ?>
		</header>

		<!-- MAIN WRAP -->
		<div class="mainwrap page-choose">
			<!-- content -->
			<div class="secondary-holder">
				<h4>Choose Language</h4>

				<div class="selections">
					<a href="register.php" class="default-link">English</a>
					<a href="register.php" class="default-link">Tagalog</a>
				</div>
			</div>
			<!-- end of content -->
			<!-- FOOTER you can move this outside the mainwrap-->
			<footer>
				<?php include('includes/footer.php'); ?>
			</footer>
		</div> <!-- end mainwrap -->

		<!-- JAVASCRIPT -->
		<?php include('includes/js.php'); ?>

	</body>
</html>
